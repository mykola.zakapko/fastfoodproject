package com.hillel.enums;

public enum BurgerType {
    CLASSIC("Classic"),
    CHEESEBURGER("CheeseBurger"),
    DOUBLECHEESEBURGER("DoubleCheeseBurger"),
    FRENCHFRY("FrenchFryBurger");

    private String name;

    BurgerType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}