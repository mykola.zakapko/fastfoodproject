package com.hillel.fastFood;

import com.hillel.enums.BurgerSize;

public abstract class BaseCondiment extends BaseBurger {

    public BaseCondiment(BurgerSize burgerSize) {
        super(burgerSize);
    }

    public abstract String getDescription();
}
