package com.hillel.fastFood;

import com.hillel.enums.BurgerSize;
import com.hillel.ingridients.FrenchFry;
import com.hillel.ingridients.Salad;

public class FrenchFryBurger extends BaseBurger {
    public FrenchFryBurger(BurgerSize burgerSize, String description) {
        super(burgerSize);
        this.description = description;
        addIngridients(new Salad(), new FrenchFry());
    }
}
