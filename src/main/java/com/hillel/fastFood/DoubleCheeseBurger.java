package com.hillel.fastFood;

import com.hillel.enums.BurgerSize;
import com.hillel.ingridients.Cheese;
import com.hillel.ingridients.Salad;

public class DoubleCheeseBurger extends BaseBurger {
    public DoubleCheeseBurger(BurgerSize burgerSize, String description) {
        super(burgerSize);
        this.description = description;
        addIngridients(new Salad(), new Cheese(), new Cheese());
    }
}
