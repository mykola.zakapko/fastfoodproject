package com.hillel.fastFood;

import com.hillel.enums.BurgerSize;
import com.hillel.ingridients.Cheese;
import com.hillel.ingridients.Salad;

public class CheeseBurger extends BaseBurger {

    public CheeseBurger(BurgerSize burgerSize, String description) {
        super(burgerSize);
        this.description = description;
        addIngridients(new Salad(), new Cheese());
    }
}
