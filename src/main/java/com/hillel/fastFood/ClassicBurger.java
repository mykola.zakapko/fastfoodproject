package com.hillel.fastFood;

import com.hillel.enums.BurgerSize;
import com.hillel.ingridients.Salad;

public class ClassicBurger extends BaseBurger {

    public ClassicBurger(BurgerSize burgerSize, String description) {
        super(burgerSize);
        this.description = description;
        addIngridients(new Salad());
    }
}
