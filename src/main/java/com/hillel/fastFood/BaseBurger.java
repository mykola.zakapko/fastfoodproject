package com.hillel.fastFood;

import com.hillel.enums.BurgerSize;
import com.hillel.exceptions.BurgerSizeException;
import com.hillel.ingridients.Ingridient;

import java.util.Arrays;

public abstract class BaseBurger {

    protected String description = "Unknown Burger";

    protected BurgerSize burgerSize;

    protected Ingridient[] ingridients;

    public BaseBurger(BurgerSize burgerSize) {
        this.burgerSize = burgerSize;
    }

    public String getDescription() {
        return description;
    }

    public double getCost() throws BurgerSizeException {
        double cost;
        switch (getBurgerSize()) {
            case SMALL:
                cost = 50.00d;
                break;
            case BIG:
                cost = 100.00d;
                break;
            default:
                throw new BurgerSizeException(getBurgerSize());
        }

        return cost + Arrays.stream(ingridients)
                .mapToDouble(Ingridient::getCost).
                        reduce(Double::sum).orElse(0.00);
    }

    public int getCalories() throws BurgerSizeException {
        int calories;
        BurgerSize burgerSize = getBurgerSize();
        switch (burgerSize) {
            case SMALL:
                calories = 20;
                break;
            case BIG:
                calories = 40;
                break;
            default:
                throw new BurgerSizeException(burgerSize);
        }

        return calories + Arrays.stream(ingridients)
                .mapToInt(Ingridient::getCalories).
                        reduce(Integer::sum).orElse(0);
    }

    public BurgerSize getBurgerSize() {
        return burgerSize;
    }


    protected void addIngridients(Ingridient... ingridients) {
        this.ingridients = ingridients;
    }
}
