package com.hillel.ingridients;

public class Cheese implements Ingridient {
    @Override
    public double getCost() {
        return 10.00d;
    }

    @Override
    public int getCalories() {
        return 20;
    }
}