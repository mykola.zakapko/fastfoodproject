package com.hillel.ingridients;

public class FrenchFry implements Ingridient {
    @Override
    public double getCost() {
        return 15.00d;
    }

    @Override
    public int getCalories() {
        return 10;
    }
}