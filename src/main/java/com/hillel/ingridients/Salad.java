package com.hillel.ingridients;

public class Salad implements Ingridient {
    @Override
    public double getCost() {
        return 20.00d;
    }

    @Override
    public int getCalories() {
        return 5;
    }
}