package com.hillel.ingridients;

public interface Ingridient {
    double getCost();

    int getCalories();
}
