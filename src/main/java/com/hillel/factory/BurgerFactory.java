package com.hillel.factory;

import com.hillel.enums.BurgerSize;
import com.hillel.enums.BurgerType;
import com.hillel.exceptions.BurgerTypeException;
import com.hillel.fastFood.BaseBurger;

public interface BurgerFactory {
    BaseBurger orderBurger(BurgerType burgerType, BurgerSize burgerSize) throws BurgerTypeException;
}