package com.hillel.factory;

import com.hillel.enums.BurgerSize;
import com.hillel.enums.BurgerType;
import com.hillel.exceptions.BurgerTypeException;
import com.hillel.fastFood.*;

public class FastFoodShop implements BurgerFactory {
    private BaseBurger baseBurger;

    @Override
    public BaseBurger orderBurger(BurgerType burgerType, BurgerSize burgerSize) throws BurgerTypeException {
        switch (burgerType) {
            case CLASSIC:
                baseBurger = new ClassicBurger(burgerSize, burgerType.getName());
                break;
            case FRENCHFRY:
                baseBurger = new FrenchFryBurger(burgerSize, burgerType.getName());
                break;
            case CHEESEBURGER:
                baseBurger = new CheeseBurger(burgerSize, burgerType.getName());
                break;
            case DOUBLECHEESEBURGER:
                baseBurger = new DoubleCheeseBurger(burgerSize, burgerType.getName());
                break;
            default:
                throw new BurgerTypeException(burgerType);
        }
        return baseBurger;
    }
}