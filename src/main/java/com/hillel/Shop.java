package com.hillel;

import com.hillel.enums.BurgerSize;
import com.hillel.enums.BurgerType;
import com.hillel.exceptions.BurgerSizeException;
import com.hillel.exceptions.BurgerTypeException;
import com.hillel.factory.FastFoodShop;
import com.hillel.fastFood.BaseBurger;
import com.hillel.supplements.Flavoring;
import com.hillel.supplements.Mayonnaise;

public class Shop {
    public static void main(String[] args) throws BurgerTypeException, BurgerSizeException {
        final FastFoodShop fastFoodShop = new FastFoodShop();
        BaseBurger baseBurger = fastFoodShop.orderBurger(BurgerType.DOUBLECHEESEBURGER, BurgerSize.SMALL);
        baseBurger = new Flavoring(baseBurger);
        baseBurger = new Mayonnaise(baseBurger);
        System.out.println(
                baseBurger.getDescription() + "\nPrice: $" +
                        baseBurger.getCost() + "\nCalories:" +
                        baseBurger.getCalories()
        );
    }
}
