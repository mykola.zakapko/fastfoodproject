package com.hillel.supplements;

import com.hillel.exceptions.BurgerSizeException;
import com.hillel.fastFood.BaseBurger;
import com.hillel.fastFood.BaseCondiment;

public class Mayonnaise extends BaseCondiment {
    private BaseBurger baseBurger;

    public Mayonnaise(BaseBurger baseBurger) {
        super(baseBurger.getBurgerSize());
        this.baseBurger = baseBurger;
    }

    @Override
    public String getDescription() {
        return baseBurger.getDescription() + ", Mayonnaise";
    }

    @Override
    public double getCost() throws BurgerSizeException {
        return baseBurger.getCost() + 20.00d;
    }

    @Override
    public int getCalories() throws BurgerSizeException {
        return baseBurger.getCalories() + 5;
    }
}
