package com.hillel.supplements;

import com.hillel.exceptions.BurgerSizeException;
import com.hillel.fastFood.BaseBurger;
import com.hillel.fastFood.BaseCondiment;

public class Flavoring extends BaseCondiment {
    private BaseBurger baseBurger;

    public Flavoring(BaseBurger baseBurger) {
        super(baseBurger.getBurgerSize());
        this.baseBurger = baseBurger;
    }

    @Override
    public String getDescription() {
        return baseBurger.getDescription() + ", Flavoring";
    }

    @Override
    public double getCost() throws BurgerSizeException {
        return baseBurger.getCost() + 15.00d;
    }

    @Override
    public int getCalories() throws BurgerSizeException {
        return baseBurger.getCalories();
    }
}
