package com.hillel.exceptions;

import com.hillel.enums.BurgerSize;

public class BurgerSizeException extends Exception {

    public BurgerSizeException(BurgerSize burgerSize) {
        super("Such burger is not exist " + burgerSize);
    }
}
