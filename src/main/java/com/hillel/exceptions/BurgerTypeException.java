package com.hillel.exceptions;

import com.hillel.enums.BurgerType;

public class BurgerTypeException extends Exception {

    public BurgerTypeException(BurgerType burgerType) {
        super("Such burger is not exist " + burgerType);
    }
}
